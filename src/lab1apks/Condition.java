/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1apks;

import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;





/**
 *
 * @author Yaroslav
 */
public class Condition extends  GElement{
    GeneralPath rhomb;
    GeneralPath trueConnector,falseConnector;
    GElement  trueOut;
    GElement  falseOut;
    Graphics2D g2;
    
    public Condition(int blockIndex,int signalIndex,int x,int y,Graphics g){
        trueConnector=new GeneralPath(GeneralPath.WIND_EVEN_ODD);
        falseConnector=new GeneralPath(GeneralPath.WIND_EVEN_ODD);
        this.g2=(Graphics2D)g;
        this.blockIndex=blockIndex;
        this.signalIndex=signalIndex;
        centralPoint=new Point2D.Double(x, y);
        redefinePoints();
        rhomb=new GeneralPath(GeneralPath.WIND_EVEN_ODD, 4);
        setRhomb();
    }
    
    private void setRhomb(){
        rhomb.moveTo(leftPoint.getX(),leftPoint.getY());
        rhomb.lineTo(topPoint.getX(),topPoint.getY());
        rhomb.lineTo(rightPoint.getX(), rightPoint.getY());
        rhomb.lineTo(botPoint.getX(), botPoint.getY());
        rhomb.closePath();
    }
    
    private void drawArrow(boolean condition){
        Line2D.Double arrow1,arrow2;
        if(condition){
        arrow1=new Line2D.Double(trueOut.getTopPoint(), 
                    new Point2D.Double(trueOut.getTopPoint().getX()-5,trueOut.getTopPoint().getY()-5));
        arrow2=new Line2D.Double(trueOut.getTopPoint(),
                    new Point2D.Double(trueOut.getTopPoint().getX()+5,trueOut.getTopPoint().getY()-5));
        }else{
            arrow1=new Line2D.Double(falseOut.getTopPoint(), 
                    new Point2D.Double(falseOut.getTopPoint().getX()-5,falseOut.getTopPoint().getY()-5));
            arrow2=new Line2D.Double(falseOut.getTopPoint(),
                    new Point2D.Double(falseOut.getTopPoint().getX()+5,falseOut.getTopPoint().getY()-5));
        }
        g2.draw(arrow1);
        g2.draw(arrow2);
    }
    @Override
    void draw() {
       g2.draw(rhomb);
       g2.drawString("X"+blockIndex, (float)centralPoint.getX()-10,(float) centralPoint.getY());
       g2.drawString("true",(float) rightPoint.getX(),(float) rightPoint.getY()-5 );
       g2.drawString("false", (float) leftPoint.getX()-15,(float) leftPoint.getY()-5);
    }

    @Override
    void drag(double x, double y) {
        centralPoint.setLocation(x,y);
        rhomb.reset();
        redefinePoints();
        setRhomb();
    }

    @Override
    boolean isConnected(GElement element) {
        if(trueOut.equals(element)||falseOut.equals(element))
            return true;
        return false;
    }

    @Override
    boolean connect(GElement element,boolean condition) {
        if(condition && trueOut==null){
            trueOut=element;
            trueOut.addInput(this);
            repaintConnetions();
            return true;
        }
        if(!condition && falseOut==null){
            falseOut=element;
            falseOut.addInput(this);
            repaintConnetions();
            return true;
        }      
        return false;
    }

    @Override
    void redraw() {
        draw();
        repaintConnetions();
    }

    @Override
    boolean isConnected() {
        if(trueOut!= null && falseOut!=null)
            return true;
        return false;
    }

    @Override
    void repaintConnetions() {
        if(trueOut!=null){
            trueConnector.reset();
            trueConnector.moveTo(rightPoint.getX(), rightPoint.getY());
            double dx=rightPoint.getX()-trueOut.getTopPoint().getX();
            double dy=rightPoint.getY()-trueOut.getTopPoint().getY();
            if(dx>=0){
                    trueConnector.lineTo(rightPoint.getX()+width/2,rightPoint.getY());
                    trueConnector.lineTo(rightPoint.getX()+width/2,rightPoint.getY()-dy-hight/2);
                    trueConnector.lineTo(trueOut.getTopPoint().getX(), rightPoint.getY()-dy-hight/2);
                    trueConnector.lineTo(trueOut.getTopPoint().getX(),trueOut.getTopPoint().getY());
            }else{
                if(dy>=0){
                    trueConnector.lineTo(rightPoint.getX()-dx/2,rightPoint.getY());
                    trueConnector.lineTo(rightPoint.getX()-dx/2,trueOut.getTopPoint().getY()-hight/2);
                    trueConnector.lineTo(trueOut.getTopPoint().getX(), trueOut.getTopPoint().getY()-hight/2);
                    trueConnector.lineTo(trueOut.getTopPoint().getX(),trueOut.getTopPoint().getY());
                }else{
                    trueConnector.lineTo(trueOut.getTopPoint().getX(),rightPoint.getY());
                    trueConnector.lineTo(trueOut.getTopPoint().getX(),trueOut.getTopPoint().getY());
                }
            }
            g2.draw(trueConnector);
            drawArrow(true);
        }
        if(falseOut!=null){
            falseConnector.reset();
            falseConnector.moveTo(leftPoint.getX(), leftPoint.getY());
            double dx=leftPoint.getX()-falseOut.getTopPoint().getX();
            double dy=leftPoint.getY()-falseOut.getTopPoint().getY();
            if(dx<=0){
                falseConnector.lineTo(leftPoint.getX()-width/2,leftPoint.getY());
                falseConnector.lineTo(leftPoint.getX()-width/2,leftPoint.getY()-dy-hight/2);
                falseConnector.lineTo(falseOut.getTopPoint().getX(), leftPoint.getY()-dy-hight/2);
                falseConnector.lineTo(falseOut.getTopPoint().getX(),falseOut.getTopPoint().getY());
            }else{
                if(dy>=0){
                    falseConnector.lineTo(leftPoint.getX()-dx/2, leftPoint.getY());
                    falseConnector.lineTo(leftPoint.getX()-dx/2, falseOut.getTopPoint().getY()-hight/2);
                    falseConnector.lineTo(falseOut.getTopPoint().getX(), falseOut.getTopPoint().getY()-hight/2);
                    falseConnector.lineTo(falseOut.getTopPoint().getX(),falseOut.getTopPoint().getY());
                }else{
                    falseConnector.lineTo(falseOut.getTopPoint().getX(),leftPoint.getY());
                    falseConnector.lineTo(falseOut.getTopPoint().getX(), falseOut.getTopPoint().getY());
                }
            }
            g2.draw(falseConnector);
            drawArrow(false);
        }
    }

    @Override
    boolean disconnect(GElement element) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    boolean isCondition() {
        return true;
    }

    @Override
    void removeOutput(GElement element) {
        if(trueOut.equals(element))
            trueOut=null;
        
        if(falseOut.equals(element))
            falseOut=null;
        
    }

    @Override
    void removeThis() {
        if(falseOut!=null)
            falseOut.removeInput(this);
        if(trueOut!=null)
            trueOut.removeInput(this);
        
        for(int i=0; i <inputs.size();++i){
            inputs.get(i).removeOutput(this);
        }
    }
    
}

