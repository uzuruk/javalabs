/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1apks;

/**
 *
 * @author Yaroslav
 */
public class ElementMatrix {
    private int[][] matrix;
    private int [] indexes;
    
    public void add(GElement element){
        if(matrix.length==0){
            matrix=new int[1][1];
            indexes=new int[1];
           
        }else{
            int[][]bufm=new int[matrix.length+1][matrix.length+1];
            int[]bufi=new int[bufm.length+1];
            for(int i=0;i<matrix.length;++i){
                for(int j=0;j<matrix.length;++j){
                    bufm[i][j]=matrix[i][j];
                }
                bufi[i]=indexes[i];
            }
            matrix=bufm;
            indexes=bufi;   
        }
        indexes[indexes.length-1]=element.getSignalIndex();
    }
    
    public int getSize(){
        return matrix.length;
    }
    
    public boolean setMatrixElement(int i,int j,int value){
        if(i<getSize()&& i>=0 && j>=0 && j<getSize()){
            matrix[i][j]=value;
        }
            return false;
    }
    
    public int getMatrixElement(int i, int j){
        if(i<getSize()&& i>=0 && j>=0 && j<getSize()){
            return matrix[i][j];
        }
            return -Integer.MIN_VALUE;
    }
    
    public int[][] getMatrix(){
        return matrix;
    }
    
    public int[] getIdexes(){
        return indexes;
    }
    
    public void removeMatrixElement(GElement element){
        int index=element.getBlockIndex();
        int [][] bufm=new int[matrix.length-1][matrix.length-1];
        int [] bufv=new int[matrix.length-1];
        for(int i=0;i<index; ++i){
            for(int j=0;j<index;++j)
                bufm[i][j]=matrix[i][j];
            for(int j=index+1; j<matrix.length;++j)
                bufm[i][j-1]=matrix[i][j];
            
            bufv[i]=indexes[i];
        }
        for(int i=index+1;i<matrix.length;++i){
            for(int j=0;j<index;++j)
                bufm[i-1][j]=matrix[i][j];
            for(int j=index+1; j<matrix.length;++j)
                bufm[i-1][j-1]=matrix[i][j];
            
            bufv[i-1]=indexes[i];
        }
        
        matrix=bufm;
        indexes=bufv;
    }
    
}
