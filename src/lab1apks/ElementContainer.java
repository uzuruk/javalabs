/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1apks;

import java.util.ArrayList;


/**
 *
 * @author Yaroslav
 */
public class ElementContainer {
    
    private static ArrayList<GElement> container=new ArrayList();
    
    public static boolean add(GElement elment){
        return container.add(elment);
    }
    
    public static GElement get(int index){
        return  container.get(index);
    } 
    
    public static  GElement get(double x,double y){
        for(int i=0; i<container.size();++i){
            if(container.get(i).havePoint(x,y))
                return container.get(i);
        }
        return null;
    }
    
    public static GElement remove(double x, double y){
        for(int i=0; i<container.size();++i){
            if(container.get(i).havePoint(x,y))
                container.remove(i);
        }
        return null;
    }
    
    public static void reDrawAll(){
       for(int i=0; i<container.size();++i){
            container.get(i).redraw();
        } 
    }
    
    
}
