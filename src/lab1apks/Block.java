/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1apks;

import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;




/**
 *
 * @author Yaroslav
 */
public class Block extends GElement{
    GeneralPath connector;
    Rectangle2D.Double figure;
    GElement  output;
    Graphics2D g2;
    public Block(int blockIndex,int signalIndex,int x,int y,Graphics g){
        connector=new GeneralPath(GeneralPath.WIND_EVEN_ODD);
        this.g2=(Graphics2D)g;
        this.blockIndex=blockIndex;
        this.signalIndex=signalIndex;
        centralPoint=new Point2D.Double(x, y);
        redefinePoints();
        figure=new Rectangle2D.Double(x-width/2, y-hight/2, width, hight);
    }
    
    private void drawArrow(){
        Line2D.Double arrow1=new Line2D.Double(output.getTopPoint(), 
                    new Point2D.Double(output.getTopPoint().getX()-5,output.getTopPoint().getY()-5));
        Line2D.Double arrow2=new Line2D.Double(output.getTopPoint(),
                    new Point2D.Double(output.getTopPoint().getX()+5,output.getTopPoint().getY()-5));
        g2.draw(arrow1);
        g2.draw(arrow2);
    }
    
    @Override
    void draw() {
       g2.draw(figure);
       g2.drawString("y"+blockIndex, (float)centralPoint.getX()-10, (float)centralPoint.getY());
    }
    @Override
    void redraw() {
        draw();
        if(isConnected()){
            repaintConnetions();
        }
    }
    
    

    @Override
    void drag(double dx, double dy) {
        centralPoint.setLocation(dx,dy);
        redefinePoints();
        figure.setRect(centralPoint.getX()-width/2,centralPoint.getY()-hight/2, width, hight);
        //redraw();
    }


    @Override
    boolean isConnected(GElement element) {
        if(output!=null && output.equals(element))
            return true;
        return false;
    }
    
    @Override
    boolean isConnected() {
        if(output!=null)
            return true;
        return false;
    }
    

    @Override
    boolean connect(GElement element,boolean condition) {
        if(element!=null && !isConnected()){
            output=element;
            element.addInput(this);
            repaintConnetions();
           return true;
        }
        return false;
    }

    @Override
    void repaintConnetions() {
        if(output!=null){
            connector.reset();
            connector.moveTo(botPoint.getX(), botPoint.getY());
            double dy=botPoint.getY()-output.getTopPoint().getY();
            double dx=botPoint.getX()-output.getTopPoint().getX();
            if(dy>=0){
                if(dx>=0){
                    connector.lineTo(botPoint.getX(), botPoint.getY()+hight/2);
                    connector.lineTo(botPoint.getX()+width,botPoint.getY()+hight/2);
                    connector.lineTo(botPoint.getX()+width,botPoint.getY()-hight-dy);
                    connector.lineTo(output.getTopPoint().getX(),output.getTopPoint().getY()-hight);
                    connector.lineTo(output.getTopPoint().getX(), output.getTopPoint().getY());  
               
                }else{
                    connector.lineTo(botPoint.getX(), botPoint.getY()+hight/2);
                    connector.lineTo(botPoint.getX()-width,botPoint.getY()+hight/2);
                    connector.lineTo(botPoint.getX()-width,botPoint.getY()-hight-dy);
                    connector.lineTo(output.getTopPoint().getX(),output.getTopPoint().getY()-hight);
                    connector.lineTo(output.getTopPoint().getX(), output.getTopPoint().getY()); 
                }
           }else{
                connector.lineTo(botPoint.getX(), botPoint.getY()-dy/2);
                connector.lineTo(botPoint.getX()-dx,botPoint.getY()-dy/2);
                connector.lineTo(output.getTopPoint().getX(),output.getTopPoint().getY());
           }
           g2.draw(connector);
           drawArrow();
        }
    }

    @Override
    boolean disconnect(GElement element) {
        if(output!=null && output.equals(element)){
            element.removeInput(this);
            connector.reset();
            output=null;
            redraw();
        }
        return false;
    }

    @Override
    boolean isCondition() {
        return false;
    }

    @Override
    void removeThis() {
        output.removeInput(this);
        for(int i=0; i<inputs.size();++i){
            inputs.get(i).removeOutput(this);
        }
    }

    @Override
    void removeOutput(GElement element) {
        if(output.equals(element))
            output=null;
    }

    

    
    
    
}
