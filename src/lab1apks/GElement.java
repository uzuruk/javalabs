/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1apks;


import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 *
 * @author Yaroslav
 */
public abstract class GElement {
    protected int blockIndex, signalIndex, 
                    hight=50, 
                    width=100;
    protected Point2D centralPoint, botPoint, topPoint, leftPoint, rightPoint;
    protected ArrayList<GElement> inputs=new ArrayList<GElement>();
    
   
    abstract void draw();
    abstract void redraw();
    abstract void drag(double x, double y);
    abstract boolean isConnected();
    abstract boolean isConnected(GElement element);
    abstract boolean connect(GElement element,boolean condition);
    abstract boolean disconnect(GElement element);
    abstract void repaintConnetions();
    abstract boolean isCondition();
    abstract void removeOutput(GElement element);
    abstract void removeThis();
    public  GElement getElement(){
        return this;
    }
    
    public Point2D getPosition(){
        return centralPoint;
    }
    
    public Point2D getTopPoint(){
        return topPoint;
    }
    
    public Point2D getBotPoint(){
        return botPoint;
    }
    
    public Point2D getLeftPoint(){
        return leftPoint;
    }
    
    public Point2D getRightPoint(){
        return rightPoint;
    }
    
    public void setBlockIndex(int index){
        blockIndex = index;
    }
    
    public void setSignalIndex(int index){
        signalIndex=index;
    }
    
    public int getBlockIndex(){
        return blockIndex;
    }
    
    public int getSignalIndex(){
        return signalIndex;
    }
    public boolean addInput(GElement element){
        return inputs.add(element);
    }
    
    public boolean removeInput(GElement element){
        return inputs.remove(element);
    }
    
    
    public ArrayList<GElement> getInputs(){
        return inputs;
    }
    
    public boolean isComeIn(GElement element){
        for(int i=0; i<inputs.size();++i){
            if(inputs.get(i).equals(element))
                return true;
        }
        return false;
    }
    
    public boolean havePoint(double x,double y){
        if(x>=leftPoint.getX()&& x<=rightPoint.getX()&&
           y>=topPoint.getY()&& y<=botPoint.getY())
                return true;
        return false;
    }
    public void redefinePoints(){
        if(topPoint==null){
            topPoint=new Point2D.Double(centralPoint.getX(),centralPoint.getY()-hight/2);
        }else{
            topPoint.setLocation(centralPoint.getX(),centralPoint.getY()-hight/2);
        }
        if(botPoint==null){
            botPoint=new Point2D.Double(centralPoint.getX(),centralPoint.getY()+hight/2);
        }else{
            botPoint.setLocation(centralPoint.getX(),centralPoint.getY()+hight/2);
        }
        if(leftPoint==null){
            leftPoint= new Point2D.Double(centralPoint.getX()-width/2, centralPoint.getY());
        }else{
            leftPoint.setLocation(centralPoint.getX()-width/2,centralPoint.getY());
        }
        if(rightPoint==null){
            rightPoint=new Point2D.Double(centralPoint.getX()+width/2, centralPoint.getY());
        }
            rightPoint.setLocation(centralPoint.getX()+width/2, centralPoint.getY());
    
    }
    
    
}
